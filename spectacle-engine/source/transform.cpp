
#include "transform.h"

namespace Spec {

	Transform::Transform(){

	}

	
	Transform::Transform(const glm::vec3& position, const glm::vec3& size, const glm::quat& orientation) :
			position(position),
			size(size),
			orientation(orientation) {

	}


	void Transform::translate(const glm::vec3& translation){
		position += translation;
		onUpdate();
	}


	void Transform::scale(const glm::vec3& scale){
		size.x *= scale.x;
		size.y *= scale.y;
		size.z *= scale.z;
		onUpdate();
	}


	void Transform::rotate(const glm::quat& rotation){
		orientation = rotation * orientation;
		onUpdate();
	}


	void Transform::setPosition(const glm::vec3& position){
		this->position = position;
		onUpdate();
	}


	void Transform::setSize(const glm::vec3& size){
		this->size = size;
		onUpdate();
	}


	void Transform::setOrientation(const glm::quat& orientation){
		this->orientation = orientation;
		onUpdate();
	}


	glm::vec3 Transform::getPosition() const {
		return position;
	}


	glm::vec3 Transform::getSize() const {
		return size;
	}


	glm::quat Transform::getOrientation() const {
		return orientation;
	}


	void Transform::onUpdate(){

	}

}
