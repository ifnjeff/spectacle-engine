#ifndef SPECTACLE_H
#define SPECTACLE_H

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include "camera.h"
#include "entity.h"
#include "keys.h"
#include "material.h"
#include "model.h"
#include "shader.h"
#include "technique.h"
#include "texture.h"
#include "transform.h"
#include "window.h"

#include <stdexcept>


namespace Spec {

	bool init();
	void update();
	void terminate();

}

#endif