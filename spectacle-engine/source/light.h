#ifndef LIGHT_H
#define LIGHT_H

#include <glm/glm.hpp>

#include "transform.h"


namespace Spec {

	class Light : public Transform {
	public:
		Light(const glm::vec3& color, float attenuation);

		void setColor(const glm::vec3& color);
		void setAttenuation(float attenuation);

		glm::vec3 getColor() const;
		float getAttenuation() const;

	private:
		glm::vec3 color;
		float attenuation;

	};

}

#endif