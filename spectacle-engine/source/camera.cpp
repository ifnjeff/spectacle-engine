
#include "camera.h"


namespace Spec {

	Camera::Camera(float nearPlane, float farPlane, float fieldOfView, float aspectRatio) :
			nearPlane(nearPlane),
			farPlane(farPlane),
			fieldOfView(fieldOfView),
			aspectRatio(aspectRatio) {
		updateProjectionMatrix();
		createUbo();
	}


	Camera::~Camera(){
		glDeleteBuffers(1, &ubo);
	}


	Camera& Camera::operator= (const Camera& other){
		if (this != &other){
			glDeleteBuffers(1, &ubo);
			
			nearPlane = other.nearPlane;
			farPlane = other.farPlane;
			fieldOfView = other.fieldOfView;
			aspectRatio = other.aspectRatio;

			position = other.position;
			size = other.size;
			orientation = other.orientation;

			viewMatrix = other.viewMatrix;
			projectionMatrix = other.projectionMatrix;
			vpMatrix = other.vpMatrix;

			createUbo();
		}

		return *this;
	}


	Camera::Camera(const Camera& other){
		nearPlane = other.nearPlane;
		farPlane = other.farPlane;
		fieldOfView = other.fieldOfView;
		aspectRatio = other.aspectRatio;

		position = other.position;
		size = other.size;
		orientation = other.orientation;

		viewMatrix = other.viewMatrix;
		projectionMatrix = other.projectionMatrix;
		vpMatrix = other.vpMatrix;

		createUbo();
	}


	void Camera::setNearPlane(float nearPlane){
		this->nearPlane = nearPlane;
		updateProjectionMatrix();
	}


	void Camera::setFarPlane(float farPlane){
		this->farPlane = farPlane;
		updateProjectionMatrix();
	}


	void Camera::setFieldOfView(float fieldOfView){
		this->fieldOfView = fieldOfView;
		updateProjectionMatrix();
	}


	void Camera::setAspectRatio(float aspectRatio){
		this->aspectRatio = aspectRatio;
		updateProjectionMatrix();
	}


	glm::mat4 Camera::getViewMatrix() const {
		return viewMatrix;
	}


	glm::mat4 Camera::getProjectionMatrix() const {
		return projectionMatrix;
	}


	glm::mat4 Camera::getVpMatrix() const {
		return vpMatrix;
	}


	void Camera::useUniforms() const {
		glBindBufferBase(GL_UNIFORM_BUFFER, UNIFORM_BINDING_CAMERA, ubo);
	}

	
	void Camera::onUpdate(){
		glm::mat4 translationMatrix = glm::translate(glm::mat4(), position);
		glm::mat4 rotationMatrix = glm::mat4_cast(orientation);

		viewMatrix = glm::inverse(translationMatrix * rotationMatrix);
		vpMatrix = projectionMatrix * viewMatrix;

		glBindBuffer(GL_UNIFORM_BUFFER, ubo);

		glBufferSubData(GL_UNIFORM_BUFFER, UNIFORM_OFFSET_VP, sizeof(glm::mat4), &vpMatrix[0][0]);
		glBufferSubData(GL_UNIFORM_BUFFER, UNIFORM_OFFSET_V, sizeof(glm::mat4), &viewMatrix[0][0]);

		glBindBuffer(GL_UNIFORM_BUFFER, 0);
	}


	void Camera::updateProjectionMatrix(){
		projectionMatrix = glm::perspective(fieldOfView, aspectRatio, nearPlane, farPlane);
		vpMatrix = projectionMatrix * viewMatrix;

		glBindBuffer(GL_UNIFORM_BUFFER, ubo);

		glBufferSubData(GL_UNIFORM_BUFFER, UNIFORM_OFFSET_VP, sizeof(glm::mat4), &vpMatrix[0][0]);
		glBufferSubData(GL_UNIFORM_BUFFER, UNIFORM_OFFSET_P, sizeof(glm::mat4), &projectionMatrix[0][0]);

		glBindBuffer(GL_UNIFORM_BUFFER, 0);
	}


	void Camera::createUbo(){
		glGenBuffers(1, &ubo);
		glBindBuffer(GL_UNIFORM_BUFFER, ubo);

		glBufferData(GL_UNIFORM_BUFFER, UNIFORM_SIZE_CAMERA, NULL, GL_DYNAMIC_DRAW);
		glBufferSubData(GL_UNIFORM_BUFFER, UNIFORM_OFFSET_VP, sizeof(glm::mat4), &vpMatrix[0][0]);
		glBufferSubData(GL_UNIFORM_BUFFER, UNIFORM_OFFSET_V, sizeof(glm::mat4), &viewMatrix[0][0]);
		glBufferSubData(GL_UNIFORM_BUFFER, UNIFORM_OFFSET_P, sizeof(glm::mat4), &projectionMatrix[0][0]);

		glBindBuffer(GL_UNIFORM_BUFFER, 0);
	}
	
}
