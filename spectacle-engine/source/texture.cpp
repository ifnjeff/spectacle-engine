
#include "texture.h"

#define STB_IMAGE_IMPLEMENTATION
#include <stb_image.h>


namespace Spec {

	Texture::Texture(const std::string& filePath){
		data = stbi_load(filePath.c_str(), &width, &height, &components, 0);

		if (!data){
			throw std::runtime_error("Failed to load image file: \"" + filePath + "\"\n" + std::string(stbi_failure_reason()));
		}

		setupTexture();
	}


	Texture::~Texture(){
		glDeleteTextures(1, &texture);
		free(data);
	}


	Texture& Texture::operator= (const Texture& other){
		if (this != &other){
			glDeleteTextures(1, &texture);
			free(data);

			width = other.width;
			height = other.height;
			components = other.components;

			data = (unsigned char*)malloc(width * height * components);
			memcpy(data, other.data, width * height * components);

			setupTexture();
		}

		return *this;
	}


	Texture::Texture(const Texture& other){
		width = other.width;
		height = other.height;
		components = other.components;

		data = (unsigned char*)malloc(width * height * components);
		memcpy(data, other.data, width * height * components);

		setupTexture();
	}


	GLuint Texture::getObject() const {
		return texture;
	}


	void Texture::useTexture(TextureUnit unit) const {
		glActiveTexture(unit);
		glBindTexture(GL_TEXTURE_2D, texture);
	}


	void Texture::setupTexture(){
		glGenTextures(1, &texture);
		glBindTexture(GL_TEXTURE_2D, texture);

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

		GLenum format;
		if (components == 1){
			format = GL_RED;
		}
		else if (components == 1){
			format = GL_RG;
		}
		else if (components == 1){
			format = GL_RGB;
		}
		else {
			format = GL_RGBA;
		}

		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, format, GL_UNSIGNED_BYTE, data);

		glBindTexture(GL_TEXTURE_2D, 0);
	}

}