#ifndef TEXTURE_H
#define TEXTURE_H

#include <GL/glew.h>

#include "technique.h"

#include <cstdlib>
#include <cstring>
#include <string>


namespace Spec {

	class Texture {
	public:
		Texture(const std::string& filePath);
		~Texture();
		Texture& operator= (const Texture& other);
		Texture(const Texture& other);

		GLuint getObject() const;

		void useTexture(TextureUnit unit) const;

	private:
		void setupTexture();

		int width;
		int height;
		int components;

		unsigned char* data;

		GLuint texture;

	};

}

#endif