#ifndef SHADER_H
#define SHADER_H

#include <GL/glew.h>

#include <fstream>
#include <stdexcept>
#include <string>


namespace Spec {

	enum ShaderType {
		VERTEX_SHADER = GL_VERTEX_SHADER,
		FRAGMENT_SHADER = GL_FRAGMENT_SHADER
	};

	class Shader {
	public:
		Shader(const std::string& filePath, ShaderType type);
		~Shader();
		Shader& operator= (const Shader& other);
		Shader(const Shader& other);

		GLuint getObject() const;

	private:
		void compileShader();

		std::string source;
		ShaderType type;

		GLuint shaderObject;

	};

}

#endif
