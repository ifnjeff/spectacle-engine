#ifndef SCENE_H
#define SCENE_H

#include <GL/glew.h>

#include "camera.h"
#include "entity.h"
#include "light.h"

#include <vector>


namespace Spec {

	class Scene {
	public:
		Scene();
		~Scene();
		Scene& operator= (const Scene& other);
		Scene(const Scene& other);

		void addEntity(const Entity& entity);
		void removeEntity(const Entity& entity);

		void addLight(const Light& light);
		void removeLight(const Light& light);

		void setActiveCamera(const Camera& camera);

		void render() const;

	private:
		void createUbo();

		GLuint ubo;

		std::vector<const Entity*> entities;
		std::vector<const Light*> lights;

		const Camera* activeCamera;

	};

}

#endif
