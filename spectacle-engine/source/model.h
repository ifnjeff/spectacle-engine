#ifndef MODEL_H
#define MODEL_H

#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>
#include <GL/glew.h>
#include <glm/glm.hpp>

#include "technique.h"

#include <stdexcept>
#include <string>
#include <vector>


namespace Spec {

	struct Vertex {
		Vertex();
		Vertex(glm::vec3 position, glm::vec3 normal, glm::vec2 uv, glm::vec3 color, glm::vec3 tangent, glm::vec3 bitangent);

		glm::vec3 position;
		glm::vec3 normal;
		glm::vec2 uv;
		glm::vec3 color;
		glm::vec3 tangent;
		glm::vec3 bitangent;
	};

	class Model {
	public:
		Model(const std::string& filePath);
		~Model();
		Model& operator= (const Model& other);
		Model(const Model& other);

		GLuint getVao() const;
		unsigned int numIndices() const;

	private:
		void assignBuffers();

		std::vector<Vertex> vertices;
		std::vector<unsigned short> indices;

		GLuint vao;
		GLuint vbo;
		GLuint ibo;

	};

}

#endif