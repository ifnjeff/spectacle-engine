
#include "entity.h"


namespace Spec {

	Entity::Entity(const Model& model, const Material& material, const Technique& technique) :
			Transform(glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(1.0f, 1.0f, 1.0f), glm::quat()) {
		this->model = &model;
		this->material = &material;
		this->technique = &technique;

		createUbo();
	}


	Entity::~Entity(){
		glDeleteBuffers(1, &ubo);
	}


	Entity& Entity::operator= (const Entity& other){
		if (this != &other){
			glDeleteBuffers(1, &ubo);

			model = other.model;
			material = other.material;
			technique = other.technique;

			position = other.position;
			size = other.size;
			orientation = other.orientation;

			modelMatrix = other.modelMatrix;

			createUbo();
		}

		return *this;
	}


	Entity::Entity(const Entity& other){
		model = other.model;
		material = other.material;
		technique = other.technique;

		position = other.position;
		size = other.size;
		orientation = other.orientation;

		modelMatrix = other.modelMatrix;

		createUbo();
	}


	void Entity::changeModel(const Model& model){
		this->model = &model;
	}


	void Entity::changeMaterial(const Material& material){
		this->material = &material;
	}


	void Entity::changeTechnique(const Technique& technique){
		this->technique = &technique;
	}


	void Entity::render(const Camera& camera) const {
		glm::mat4 mvMatrix = camera.getViewMatrix() * modelMatrix;
		glm::mat4 mvpMatrix = camera.getProjectionMatrix() * mvMatrix;
		glm::mat4 mv_tMatrix = glm::transpose(mvMatrix);
		glm::mat4 mv_itMatrix = glm::inverse(mv_tMatrix);

		glBindBuffer(GL_UNIFORM_BUFFER, ubo);

		glBufferSubData(GL_UNIFORM_BUFFER, UNIFORM_OFFSET_MVP, sizeof(glm::mat4), &mvpMatrix[0][0]);
		glBufferSubData(GL_UNIFORM_BUFFER, UNIFORM_OFFSET_MV, sizeof(glm::mat4), &mvMatrix[0][0]);
		glBufferSubData(GL_UNIFORM_BUFFER, UNIFORM_OFFSET_M, sizeof(glm::mat4), &modelMatrix[0][0]);
		glBufferSubData(GL_UNIFORM_BUFFER, UNIFORM_OFFSET_MV_T, sizeof(glm::mat4), &mv_tMatrix[0][0]);
		glBufferSubData(GL_UNIFORM_BUFFER, UNIFORM_OFFSET_MV_IT, sizeof(glm::mat4), &mv_itMatrix[0][0]);

		glBindBuffer(GL_UNIFORM_BUFFER, 0);

		technique->use();
		material->useUniforms();
		camera.useUniforms();
		glBindBufferBase(GL_UNIFORM_BUFFER, UNIFORM_BINDING_ENTITY, ubo);

		glBindVertexArray(model->getVao());
		glDrawElements(GL_TRIANGLES, model->numIndices(), GL_UNSIGNED_SHORT, 0);
		glBindVertexArray(0);
	}


	void Entity::createUbo(){
		glGenBuffers(1, &ubo);
		glBindBuffer(GL_UNIFORM_BUFFER, ubo);

		glBufferData(GL_UNIFORM_BUFFER, UNIFORM_SIZE_ENTITY, NULL, GL_DYNAMIC_DRAW);

		glBindBuffer(GL_UNIFORM_BUFFER, 0);
	}


	void Entity::onUpdate(){
		glm::mat4 translationMatrix = glm::translate(glm::mat4(), position);
		glm::mat4 scaleMatrix = glm::scale(glm::mat4(), size);
		glm::mat4 rotationMatrix = glm::mat4_cast(orientation);

		modelMatrix = translationMatrix * rotationMatrix * scaleMatrix;
	}

}