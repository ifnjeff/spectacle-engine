#ifndef WINDOW_H
#define WINDOW_H

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>

#include "keys.h"
#include "scene.h"

#include <iostream>
#include <stdexcept>
#include <string>
#include <vector>


namespace Spec {

	typedef void(*KeyCallbackType)(Key, int, Action, Modifiers);
	typedef void(*CursorPositionCallbackType)(double, double);
	typedef void(*MouseButtonCallbackType)(Button, Action, Modifiers);
	typedef void(*ResizeCallbackType)(int, int);
	

	enum WindowType {
		WINDOWED,
		FULLSCREEN,
		FULLSCREEN_BORDERLESS
	};

	class Window {
	public:
		Window(int width, int height, const std::string& title, WindowType type);
		~Window();
		Window& operator= (const Window& other);
		Window(const Window& other);
		
		void setSize(int width, int height);
		void setTitle(const std::string& title);
		void setBackgroundColor(float r, float g, float b);

		void disableCursor();
		void enableCursor();

		void setActiveScene(const Scene& scene);

		void setKeyCallback(KeyCallbackType function);
		void setCursorPositionCallback(CursorPositionCallbackType function);
		void setMouseButtonCallback(MouseButtonCallbackType function);
		void setResizeCallback(ResizeCallbackType function);

		KeyCallbackType getKeyCallback() const;
		CursorPositionCallbackType getCursorPositionCallback() const;
		MouseButtonCallbackType getMouseButtonCallback() const;
		ResizeCallbackType getResizeCallback() const;

		bool getKey(Key key) const;
		bool getMouseButton(Button button) const;
		glm::dvec2 getCursorPosition() const;

		GLFWwindow* getObject() const;

		void render();

		double deltaTime() const;

		bool shouldClose() const;

	private:
		void createWindow(int width, int height, const std::string& title, WindowType type);

		GLFWwindow* window;
		std::string title;
		WindowType type;

		const Scene* activeScene;

		KeyCallbackType keyCallback;
		CursorPositionCallbackType cursorPositionCallback;
		MouseButtonCallbackType mouseButtonCallback;
		ResizeCallbackType resizeCallback;

		double lastTime;
		double previousDeltaTime;

	};

	class WindowManager {
	public:
		static void keyCallback(GLFWwindow* window, int key, int scancode, int action, int mods);
		static void cursorPositionCallback(GLFWwindow* window, double x, double y);
		static void mouseButtonCallback(GLFWwindow* window, int button, int action, int mods);
		static void resizeCallback(GLFWwindow* window, int width, int height);

		static void registerWindow(Window* window);
		static void deregisterWindow(Window* window);

	private:
		WindowManager();

		static std::vector<Window*> windows;

	};

}

#endif