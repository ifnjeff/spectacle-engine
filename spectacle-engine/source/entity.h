#ifndef ENTITY_H
#define ENTITY_H

#include <GL/glew.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/quaternion.hpp>

#include "camera.h"
#include "material.h"
#include "model.h"
#include "technique.h"
#include "transform.h"


namespace Spec {

	class Entity : public Transform {
	public:
		Entity(const Model& model, const Material& material, const Technique& technique);
		~Entity();
		Entity& operator= (const Entity& other);
		Entity(const Entity& other);

		void changeModel(const Model& model);
		void changeMaterial(const Material& material);
		void changeTechnique(const Technique& technique);

		void render(const Camera& camera) const;

	private:
		virtual void onUpdate();

		void createUbo();

		GLuint ubo;

		const Model* model;
		const Material* material;
		const Technique* technique;

		glm::mat4 modelMatrix;
		
	};

}

#endif