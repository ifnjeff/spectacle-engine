
#include "window.h"


namespace Spec {

	Window::Window(int width, int height, const std::string& title, WindowType type) :
			title(title),
			type(type),
			keyCallback(NULL),
			cursorPositionCallback(NULL),
			mouseButtonCallback(NULL),
			resizeCallback(NULL) {
		createWindow(width, height, title, type);
	}


	Window::~Window(){
		glfwDestroyWindow(window);
		WindowManager::deregisterWindow(this);
	}


	Window& Window::operator= (const Window& other){
		if (this != &other){
			int width, height;
			glfwGetWindowSize(other.window, &width, &height);

			keyCallback = other.keyCallback;
			cursorPositionCallback = other.cursorPositionCallback;
			mouseButtonCallback = other.mouseButtonCallback;
			resizeCallback = other.resizeCallback;

			glfwDestroyWindow(window);
			WindowManager::deregisterWindow(this);
			createWindow(width, height, other.title, other.type);
		}

		return *this;
	}


	Window::Window(const Window& other){
		int width, height;
		glfwGetWindowSize(other.window, &width, &height);

		keyCallback = other.keyCallback;
		cursorPositionCallback = other.cursorPositionCallback;
		mouseButtonCallback = other.mouseButtonCallback;
		resizeCallback = other.resizeCallback;

		createWindow(width, height, other.title, other.type);
	}


	void Window::setSize(int width, int height){
		glfwSetWindowSize(window, width, height);
	}


	void Window::setTitle(const std::string& title){
		glfwSetWindowTitle(window, title.c_str());
	}


	void Window::setBackgroundColor(float r, float g, float b){
		glfwMakeContextCurrent(window);
		glClearColor(r, g, b, 1.0f);
	}


	void Window::disableCursor(){
		glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
	}


	void Window::enableCursor(){
		glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
	}


	void Window::setActiveScene(const Scene& scene){
		activeScene = &scene;
	}


	void Window::setKeyCallback(KeyCallbackType function){
		keyCallback = function;
	}


	void Window::setCursorPositionCallback(CursorPositionCallbackType function){
		cursorPositionCallback = function;
	}


	void Window::setMouseButtonCallback(MouseButtonCallbackType function){
		mouseButtonCallback = function;
	}

	
	void Window::setResizeCallback(ResizeCallbackType function){
		resizeCallback = function;
	}


	KeyCallbackType Window::getKeyCallback() const {
		return keyCallback;
	}

	
	CursorPositionCallbackType Window::getCursorPositionCallback() const {
		return cursorPositionCallback;
	}


	MouseButtonCallbackType Window::getMouseButtonCallback() const {
		return mouseButtonCallback;
	}


	ResizeCallbackType Window::getResizeCallback() const {
		return resizeCallback;
	}


	bool Window::getKey(Key key) const {
		return (glfwGetKey(window, key)) ? true : false;
	}


	bool Window::getMouseButton(Button button) const {
		return (glfwGetMouseButton(window, button)) ? true : false;
	}


	glm::dvec2 Window::getCursorPosition() const {
		glm::dvec2 position;
		glfwGetCursorPos(window, &position.x, &position.y);

		return position;
	}


	GLFWwindow* Window::getObject() const {
		return window;
	}


	void Window::render(){
		glfwMakeContextCurrent(window);

		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		if (activeScene != NULL){
			activeScene->render();
		}

		glfwSwapBuffers(window);

		double currentTime = glfwGetTime();
		previousDeltaTime = currentTime - lastTime;
		lastTime = currentTime;
	}


	double Window::deltaTime() const {
		return previousDeltaTime;
	}


	bool Window::shouldClose() const {
		return (glfwWindowShouldClose(window)) ? true : false;
	}


	void Window::createWindow(int width, int height, const std::string& title, WindowType type){
		GLFWmonitor* monitor = glfwGetPrimaryMonitor();
		const GLFWvidmode* mode = glfwGetVideoMode(monitor);

		glfwWindowHint(GLFW_RED_BITS, mode->redBits);
		glfwWindowHint(GLFW_GREEN_BITS, mode->greenBits);
		glfwWindowHint(GLFW_BLUE_BITS, mode->blueBits);
		glfwWindowHint(GLFW_REFRESH_RATE, mode->refreshRate);

		glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
		glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
		glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
		glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
		glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);

		if (type == FULLSCREEN_BORDERLESS){
			window = glfwCreateWindow(mode->width, mode->height, title.c_str(), monitor, NULL);
		}
		else if (type == FULLSCREEN){
			window = glfwCreateWindow(width, height, title.c_str(), monitor, NULL);
		}
		else {
			window = glfwCreateWindow(width, height, title.c_str(), NULL, NULL);
		}

		if (window == NULL){
			throw std::runtime_error("Failed to create window (Is video mode valid?)");
		}

		WindowManager::registerWindow(this);
		glfwSetKeyCallback(window, WindowManager::keyCallback);
		glfwSetCursorPosCallback(window, WindowManager::cursorPositionCallback);
		glfwSetMouseButtonCallback(window, WindowManager::mouseButtonCallback);
		glfwSetWindowSizeCallback(window, WindowManager::resizeCallback);

		glfwMakeContextCurrent(window);
		glfwSwapInterval(1);

		glewExperimental = GL_TRUE;
		GLenum errorCode = glewInit();
		if (errorCode != GLEW_OK){
			glfwTerminate();
			throw std::runtime_error("Failed to initialize GLFW: " + std::string((const char*)glewGetErrorString(errorCode)));
		}

		glEnable(GL_CULL_FACE);
		glCullFace(GL_BACK);
		glEnable(GL_DEPTH_TEST);
		glDepthFunc(GL_LESS);

		glClearColor(0.0f, 0.0f, 0.15f, 1.0f);

		std::cout << "OpenGL version: " << glGetString(GL_VERSION) << std::endl;
		std::cout << "GLSL version: " << glGetString(GL_SHADING_LANGUAGE_VERSION) << std::endl;
		std::cout << "Graphics card: " << glGetString(GL_RENDERER) << std::endl;

		lastTime = glfwGetTime();
		previousDeltaTime = 0;
	}


	std::vector<Window*> WindowManager::windows;


	void WindowManager::keyCallback(GLFWwindow* window, int key, int scancode, int action, int mods){
		for (unsigned i = 0; i < windows.size(); i++){
			if (windows.at(i)->getObject() == window){
				KeyCallbackType callback = windows.at(i)->getKeyCallback();
				if (callback != NULL){
					callback(static_cast<Key>(key), scancode, static_cast<Action>(action), static_cast<Modifiers>(mods));
				}
			}
		}
	}


	void WindowManager::cursorPositionCallback(GLFWwindow* window, double x, double y){
		for (unsigned i = 0; i < windows.size(); i++){
			if (windows.at(i)->getObject() == window){
				CursorPositionCallbackType callback = windows.at(i)->getCursorPositionCallback();
				if (callback != NULL){
					callback(x, y);
				}
			}
		}
	}


	void WindowManager::mouseButtonCallback(GLFWwindow* window, int button, int action, int mods){
		for (unsigned i = 0; i < windows.size(); i++){
			if (windows.at(i)->getObject() == window){
				MouseButtonCallbackType callback = windows.at(i)->getMouseButtonCallback();
				if (callback != NULL){
					callback(static_cast<Button>(button), static_cast<Action>(action), static_cast<Modifiers>(mods));
				}
			}
		}
	}

	
	void WindowManager::resizeCallback(GLFWwindow* window, int width, int height){
		for (unsigned i = 0; i < windows.size(); i++){
			if (windows.at(i)->getObject() == window){
				ResizeCallbackType callback = windows.at(i)->getResizeCallback();
				if (callback != NULL){
					callback(width, height);
				}
			}
		}
	}


	void WindowManager::registerWindow(Window* window){
		for (unsigned i = 0; i < windows.size(); i++){
			if (windows.at(i) == window){
				return;
			}
		}

		windows.push_back(window);
	}


	void WindowManager::deregisterWindow(Window* window){
		for (unsigned i = 0; i < windows.size(); i++){
			if (windows.at(i) == window){
				windows.erase(windows.begin() + i);
				return;
			}
		}
	}

}