
#include "model.h"


namespace Spec {

	Vertex::Vertex(){

	}


	Vertex::Vertex(glm::vec3 position, glm::vec3 normal, glm::vec2 uv, glm::vec3 color, glm::vec3 tangent, glm::vec3 bitangent) :
			position(position),
			normal(normal),
			uv(uv),
			color(color),
			tangent(tangent),
			bitangent(bitangent) {

	}


	Model::Model(const std::string& filePath){
		Assimp::Importer importer;

		importer.ReadFile(filePath,
			aiProcess_JoinIdenticalVertices |
			aiProcess_Triangulate |
			aiProcess_GenSmoothNormals |
			aiProcess_GenUVCoords);
		
		const aiScene* scene = importer.ApplyPostProcessing(aiProcess_CalcTangentSpace | aiProcess_FlipUVs);

		if (!scene){
			throw std::runtime_error("Failed to load model file: \"" + filePath + "\"");
		}

		if (scene->mNumMeshes < 1){
			throw std::runtime_error("Could not find any meshes in model file: \"" + filePath + "\"");
		}

		const aiMesh* mesh = scene->mMeshes[0];

		for (unsigned int i = 0; i < mesh->mNumVertices; i++){
			Vertex v(	
				glm::vec3(mesh->mVertices[i].x, mesh->mVertices[i].y, mesh->mVertices[i].z),
				glm::vec3(mesh->mNormals[i].x, mesh->mNormals[i].y, mesh->mNormals[i].z), 
				(mesh->HasTextureCoords(0)) ? glm::vec2(mesh->mTextureCoords[0][i].x, mesh->mTextureCoords[0][i].y) : glm::vec2(),
				(mesh->HasVertexColors(0)) ? glm::vec3(mesh->mColors[0][i].r, mesh->mColors[0][i].g, mesh->mColors[0][i].b) : glm::vec3(1.0f, 1.0f, 1.0f),
				(mesh->HasTangentsAndBitangents()) ? glm::vec3(mesh->mTangents[i].x, mesh->mTangents[i].y, mesh->mTangents[i].z) : glm::vec3(),
				(mesh->HasTangentsAndBitangents()) ? glm::vec3(mesh->mBitangents[i].x, mesh->mBitangents[i].y, mesh->mBitangents[i].z) : glm::vec3());

			vertices.push_back(v);
		}

		for (unsigned int i = 0; i < mesh->mNumFaces; i++){
			for (unsigned int j = 0; j < mesh->mFaces[i].mNumIndices; j++){
				indices.push_back(mesh->mFaces[i].mIndices[j]);
			}
		}

		assignBuffers();
	}


	Model::~Model(){
		glDeleteBuffers(1, &vbo);
		glDeleteBuffers(1, &ibo);
		glDeleteVertexArrays(1, &vao);
	}


	Model& Model::operator= (const Model& other){
		if (this != &other){
			glDeleteBuffers(1, &vbo);
			glDeleteBuffers(1, &ibo);
			glDeleteVertexArrays(1, &vao);

			vertices = other.vertices;
			indices = other.indices;

			assignBuffers();
		}

		return *this;
	}


	Model::Model(const Model& other){
		vertices = other.vertices;
		indices = other.indices;

		assignBuffers();
	}


	GLuint Model::getVao() const{
		return vao;
	}


	unsigned int Model::numIndices() const{
		return indices.size();
	}


	void Model::assignBuffers(){
		glGenVertexArrays(1, &vao);
		glBindVertexArray(vao);

		glGenBuffers(1, &vbo);
		glBindBuffer(GL_ARRAY_BUFFER, vbo);

		glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(Vertex), &vertices[0], GL_STATIC_DRAW);

		glEnableVertexAttribArray(ATTRIB_BINDING_POSITION);
		glEnableVertexAttribArray(ATTRIB_BINDING_NORMAL);
		glEnableVertexAttribArray(ATTRIB_BINDING_UV);
		glEnableVertexAttribArray(ATTRIB_BINDING_COLOR);
		glEnableVertexAttribArray(ATTRIB_BINDING_TANGENT);
		glEnableVertexAttribArray(ATTRIB_BINDING_BITANGENT);

		glVertexAttribPointer(ATTRIB_BINDING_POSITION, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, position));
		glVertexAttribPointer(ATTRIB_BINDING_NORMAL, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, normal));
		glVertexAttribPointer(ATTRIB_BINDING_UV, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, uv));
		glVertexAttribPointer(ATTRIB_BINDING_COLOR, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, color));
		glVertexAttribPointer(ATTRIB_BINDING_TANGENT, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, tangent));
		glVertexAttribPointer(ATTRIB_BINDING_BITANGENT, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, bitangent));

		glBindBuffer(GL_ARRAY_BUFFER, 0);

		glGenBuffers(1, &ibo);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(unsigned short), &indices[0], GL_STATIC_DRAW);

		glBindVertexArray(0);
	}

}
