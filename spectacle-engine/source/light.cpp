
#include "light.h"


namespace Spec {

	Light::Light(const glm::vec3& color, float attenuation) :
			color(color),
			attenuation(attenuation) {

	}


	void Light::setColor(const glm::vec3& color){
		this->color = color;
	}


	void Light::setAttenuation(float attenuation){
		this->attenuation = attenuation;
	}


	glm::vec3 Light::getColor() const{
		return color;
	}


	float Light::getAttenuation() const{
		return attenuation;
	}

}
