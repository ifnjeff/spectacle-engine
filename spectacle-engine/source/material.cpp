
#include "material.h"


namespace Spec {

	Material::Material(glm::vec3 specularColor, float shininess, const Texture* diffuseMap, const Texture* normalMap, const Texture* specularMap) :
			specularColor(specularColor),
			shininess(shininess) {
		this->diffuseMap = diffuseMap;
		this->normalMap = normalMap;
		this->specularMap = specularMap;
		createUbo();
	}


	Material::~Material(){
		glDeleteBuffers(1, &ubo);
	}


	Material& Material::operator= (const Material& other){
		if (this != &other){
			glDeleteBuffers(1, &ubo);

			specularColor = other.specularColor;
			shininess = other.shininess;

			diffuseMap = other.diffuseMap;
			normalMap = other.normalMap;
			specularMap = other.specularMap;

			createUbo();
		}

		return *this;
	}


	Material::Material(const Material& other){
		specularColor = other.specularColor;
		shininess = other.shininess;

		diffuseMap = other.diffuseMap;
		normalMap = other.normalMap;
		specularMap = other.specularMap;

		createUbo();
	}


	void Material::changeSpecularColor(glm::vec3 specularColor){
		this->specularColor = specularColor;

		glBindBuffer(GL_UNIFORM_BUFFER, ubo);

		glBufferSubData(GL_UNIFORM_BUFFER, UNIFORM_OFFSET_SPEC_COLOR, sizeof(glm::vec3), &specularColor[0]);

		glBindBuffer(GL_UNIFORM_BUFFER, 0);
	}


	void Material::changeShininess(float shininess){
		this->shininess = shininess;

		glBindBuffer(GL_UNIFORM_BUFFER, ubo);

		glBufferSubData(GL_UNIFORM_BUFFER, UNIFORM_OFFSET_SHININESS, sizeof(float), &shininess);

		glBindBuffer(GL_UNIFORM_BUFFER, 0);
	}


	void Material::changeDiffuseMap(const Texture& diffuseMap){
		this->diffuseMap = &diffuseMap;
	}


	void Material::changeNormalMap(const Texture& normalMap){
		this->normalMap = &normalMap;
	}


	void Material::changeSpecularMap(const Texture& specularMap){
		this->specularMap = &specularMap;
	}


	glm::vec3 Material::getSpecularColor() const {
		return specularColor;
	}


	float Material::getShininess() const {
		return shininess;
	}


	void Material::useUniforms() const {
		glBindBufferBase(GL_UNIFORM_BUFFER, UNIFORM_BINDING_MATERIAL, ubo);

		if (diffuseMap != NULL){
			diffuseMap->useTexture(TEXTURE_UNIT_DIFFUSE);
		}
		if (normalMap != NULL){
			normalMap->useTexture(TEXTURE_UNIT_NORMAL);
		}
		if (specularMap != NULL){
			specularMap->useTexture(TEXTURE_UNIT_SPECULAR);
		}
	}

	
	void Material::createUbo(){
		glGenBuffers(1, &ubo);
		glBindBuffer(GL_UNIFORM_BUFFER, ubo);

		glBufferData(GL_UNIFORM_BUFFER, UNIFORM_SIZE_CAMERA, NULL, GL_DYNAMIC_DRAW);
		glBufferSubData(GL_UNIFORM_BUFFER, UNIFORM_OFFSET_SPEC_COLOR, sizeof(glm::vec3), &specularColor[0]);
		glBufferSubData(GL_UNIFORM_BUFFER, UNIFORM_OFFSET_SHININESS, sizeof(float), &shininess);

		glBindBuffer(GL_UNIFORM_BUFFER, 0);
	}

}