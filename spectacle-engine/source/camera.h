#ifndef CAMERA_H
#define CAMERA_H

#include <GL/glew.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/quaternion.hpp>

#include "technique.h"
#include "transform.h"


namespace Spec {

	class Camera : public Transform {
	public:
		Camera(float nearPlane, float farPlane, float fieldOfView, float aspectRatio);
		~Camera();
		Camera& operator= (const Camera& other);
		Camera(const Camera& other);

		void setNearPlane(float nearPlane);
		void setFarPlane(float farPlane);
		void setFieldOfView(float fieldOfView);
		void setAspectRatio(float aspectRatio);

		glm::mat4 getViewMatrix() const;
		glm::mat4 getProjectionMatrix() const;
		glm::mat4 getVpMatrix() const;

		void useUniforms() const;

	private:
		virtual void onUpdate();
		void updateProjectionMatrix();

		void createUbo();

		GLuint ubo;

		float nearPlane;
		float farPlane;
		float fieldOfView;
		float aspectRatio;

		glm::mat4 viewMatrix;
		glm::mat4 projectionMatrix;
		glm::mat4 vpMatrix;

	};

}

#endif