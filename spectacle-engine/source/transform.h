#ifndef TRANSFORM_H
#define TRANSFORM_H

#include <glm/glm.hpp>
#include <glm/gtc/quaternion.hpp>


namespace Spec {

	class Transform {
	public:
		Transform();
		Transform(const glm::vec3& position, const glm::vec3& size, const glm::quat& orientation);

		void translate(const glm::vec3& translation);
		void scale(const glm::vec3& scale);
		void rotate(const glm::quat& rotation);

		void setPosition(const glm::vec3& position);
		void setSize(const glm::vec3& size);
		void setOrientation(const glm::quat& orientation);

		glm::vec3 getPosition() const;
		glm::vec3 getSize() const;
		glm::quat getOrientation() const;

	protected:
		virtual void onUpdate();

		glm::vec3 position;
		glm::vec3 size;
		glm::quat orientation;

	};

}

#endif
