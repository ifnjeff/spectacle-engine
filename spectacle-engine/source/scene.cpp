
#include "scene.h"


namespace Spec {

	Scene::Scene(){
		createUbo();
	}


	Scene::~Scene(){
		glDeleteBuffers(1, &ubo);
	}


	Scene& Scene::operator= (const Scene& other){
		if (this != &other){
			glDeleteBuffers(1, &ubo);

			entities = other.entities;
			lights = other.lights;

			activeCamera = other.activeCamera;

			createUbo();
		}

		return *this;
	}


	Scene::Scene(const Scene& other){
		entities = other.entities;
		lights = other.lights;

		activeCamera = other.activeCamera;

		createUbo();
	}

	void Scene::addEntity(const Entity& entity){
		for (unsigned i = 0; i < entities.size(); i++){
			if (entities.at(i) == &entity){
				return;
			}
		}

		entities.push_back(&entity);
	}


	void Scene::removeEntity(const Entity& entity){
		for (unsigned i = 0; i < entities.size(); i++){
			if (entities.at(i) == &entity){
				entities.erase(entities.begin() + i);
			}
		}
	}


	void Scene::addLight(const Light& light){
		if (lights.size() < 8){
			for (unsigned i = 0; i < lights.size(); i++){
				if (lights.at(i) == &light){
					return;
				}
			}

			lights.push_back(&light);
		}
	}


	void Scene::removeLight(const Light& light){
		for (unsigned i = 0; i < lights.size(); i++){
			if (lights.at(i) == &light){
				lights.erase(lights.begin() + i);
			}
		}
	}


	void Scene::setActiveCamera(const Camera& camera){
		activeCamera = &camera;
	}


	void Scene::render() const {
		int numLights = lights.size();

		glBindBuffer(GL_UNIFORM_BUFFER, ubo);
		glBufferSubData(GL_UNIFORM_BUFFER, UNIFORM_OFFSET_NUM_LIGHTS, sizeof(int), &numLights);

		for (int i = 0; i < numLights; i++){
			glm::vec3 color = lights.at(i)->getColor();
			glm::vec3 position = glm::vec3(activeCamera->getViewMatrix() * glm::vec4(lights.at(i)->getPosition(), 1.0f));
			float attenuation = lights.at(i)->getAttenuation();

			glBufferSubData(GL_UNIFORM_BUFFER, (UNIFORM_SIZE_LIGHT_STRUCT * (numLights - 1)) + UNIFORM_OFFSET_LIGHT_COLOR, sizeof(glm::vec3), &color);
			glBufferSubData(GL_UNIFORM_BUFFER, (UNIFORM_SIZE_LIGHT_STRUCT * (numLights - 1)) + UNIFORM_OFFSET_LIGHT_POSITION, sizeof(glm::vec3), &position);
			glBufferSubData(GL_UNIFORM_BUFFER, (UNIFORM_SIZE_LIGHT_STRUCT * (numLights - 1)) + UNIFORM_OFFSET_LIGHT_ATTENUATION, sizeof(float), &attenuation);
		}

		glBindBuffer(GL_UNIFORM_BUFFER, 0);

		glBindBufferBase(GL_UNIFORM_BUFFER, UNIFORM_BINDING_LIGHTS, ubo);

		if (activeCamera != NULL){
			for (unsigned i = 0; i < entities.size(); i++){
				entities.at(i)->render(*activeCamera);
				return;
			}
		}
	}


	void Scene::createUbo() {
		glGenBuffers(1, &ubo);
		glBindBuffer(GL_UNIFORM_BUFFER, ubo);

		glBufferData(GL_UNIFORM_BUFFER, UNIFORM_SIZE_LIGHTS, NULL, GL_DYNAMIC_DRAW);

		glBindBuffer(GL_UNIFORM_BUFFER, 0);
	}

}
