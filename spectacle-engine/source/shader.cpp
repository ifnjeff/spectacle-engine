
#include "shader.h"


namespace Spec {

	Shader::Shader(const std::string& filePath, ShaderType type) :
			type(type) {
		source =
			"#version 330\n"
			"layout(std140) uniform EntityUniforms {\n"
			"	mat4 mvp;\n"
			"	mat4 mv;\n"
			"	mat4 m;\n"
			"	mat4 mv_t;\n"
			"	mat4 mv_it;\n"
			"};\n"
			"layout(std140) uniform CameraUniforms {\n"
			"	mat4 vp;\n"
			"	mat4 v;\n"
			"	mat4 p;\n"
			"};\n"
			"layout(std140) uniform MaterialUniforms {\n"
			"	vec3 specularColor;\n"
			"	float shininess;\n"
			"};\n"
			"layout(std140) uniform LightsUniforms {\n"
			"	int numLights;\n"
			"	vec3 lightColors[8];\n"
			"	vec3 lightPositions[8];\n"
			"	float lightAttenuations[8];\n"
			"};\n"
			"uniform sampler2D diffuseMap;\n"
			"uniform sampler2D normalMap;\n"
			"uniform sampler2D specularMap;\n\n";

		if (type == VERTEX_SHADER){
			source +=
				"in vec3 vertPosition;\n"
				"in vec3 vertNormal;\n"
				"in vec2 vertUv;\n"
				"in vec3 vertColor;\n"
				"in vec3 vertTangent;\n"
				"in vec3 vertBitangent;\n\n";
		}

		int headerSize = source.size();

		std::ifstream fileStream(filePath);
		if (fileStream.is_open()){
			fileStream.seekg(0, std::ios::end);
			source.resize((size_t)fileStream.tellg() + (size_t)headerSize);

			fileStream.seekg(0, std::ios::beg);
			fileStream.read(&source[headerSize], source.size() - headerSize);
			fileStream.close();
		}
		else {
			throw std::runtime_error("Could not open shader file: \"" + filePath + "\"");
		}

		compileShader();
	}


	Shader::~Shader(){
		glDeleteShader(shaderObject);
	}


	Shader& Shader::operator= (const Shader& other){
		if (this != &other){
			glDeleteShader(shaderObject);

			type = other.type;
			source = other.source;

			compileShader();
		}

		return *this;
	}


	Shader::Shader(const Shader& other){
		type = other.type;
		source = other.source;

		compileShader();
	}


	GLuint Shader::getObject() const {
		return shaderObject;
	}


	void Shader::compileShader(){
		shaderObject = glCreateShader(type);
		if (shaderObject == 0){
			throw std::runtime_error("Failed to create shader object");
		}

		const GLchar* sourceData = &source[0];
		const GLint sourceLength = source.size();

		glShaderSource(shaderObject, 1, &sourceData, &sourceLength);
		glCompileShader(shaderObject);

		GLint compileStatus;
		glGetShaderiv(shaderObject, GL_COMPILE_STATUS, &compileStatus);
		if (compileStatus == GL_FALSE){
			std::string errorMessage = "Failed to compile shader:\n";

			GLint infoLogLength;
			glGetShaderiv(shaderObject, GL_INFO_LOG_LENGTH, &infoLogLength);
			char* infoLog = new char[infoLogLength];
			glGetShaderInfoLog(shaderObject, infoLogLength, NULL, infoLog);
			errorMessage += infoLog;
			delete[] infoLog;

			glDeleteShader(shaderObject);
			shaderObject = 0;
			throw std::runtime_error(errorMessage);
		}
	}

}
