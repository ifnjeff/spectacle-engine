
#include "spectacle.h"

#include <iostream>
#include <stdexcept>

int main(int argc, const char** argv){
	try {
		Spec::init();

		Spec::Window window(1024, 768, "Spectacle Engine", Spec::WINDOWED);

		Spec::Shader vertexShader("../spectacle-engine/data/shaders/blinn-phong.vert", Spec::VERTEX_SHADER);
		Spec::Shader fragmentShader("../spectacle-engine/data/shaders/blinn-phong.frag", Spec::FRAGMENT_SHADER);

		Spec::Technique technique(vertexShader, fragmentShader);
		Spec::Model model("../spectacle-engine/data/models/bunny.dae");
		Spec::Texture diffuseMap("../spectacle-engine/data/models/bunny_diffuse.png");
		Spec::Material material(glm::vec3(1.0f, 1.0f, 1.0f), 16.0f, &diffuseMap, NULL, NULL);

		Spec::Entity entity(model, material, technique);
		entity.translate(glm::vec3(0, 0, -6));

		Spec::Light light(glm::vec3(1.0f, 1.0f, 1.0f), 0.1);
		light.translate(glm::vec3(5.0f, 3.0f, 5.0f));

		Spec::Camera camera(0.1f, 100.0f, 45.0f, 4.0f / 3.0f);

		Spec::Scene scene;
		scene.setActiveCamera(camera);
		scene.addEntity(entity);
		scene.addLight(light);

		window.setActiveScene(scene);
		window.disableCursor();

		glm::dvec2 lastCursorPosition = window.getCursorPosition();
		

		while (!(window.shouldClose() || window.getKey(Spec::KEY_ESCAPE))){
			Spec::update();

			if (window.getKey(Spec::KEY_W)){
				camera.translate(camera.getOrientation() * glm::vec3(0, 0, -3) * (float)window.deltaTime());
			}
			if (window.getKey(Spec::KEY_S)){
				camera.translate(camera.getOrientation() * glm::vec3(0, 0, 3) * (float)window.deltaTime());
			}
			if (window.getKey(Spec::KEY_A)){
				camera.translate(camera.getOrientation() * glm::vec3(-3, 0, 0) * (float)window.deltaTime());
			}
			if (window.getKey(Spec::KEY_D)){
				camera.translate(camera.getOrientation() * glm::vec3(3, 0, 0) * (float)window.deltaTime());
			}


			glm::dvec2 cursorPosition = window.getCursorPosition();

			glm::quat rotation = glm::angleAxis(-0.003f * (float)(cursorPosition.y - lastCursorPosition.y), camera.getOrientation() * glm::vec3(1, 0, 0));
			rotation = glm::angleAxis(-0.003f * (float)(cursorPosition.x - lastCursorPosition.x), glm::vec3(0, 1, 0)) * rotation;
			camera.rotate(rotation);

			lastCursorPosition = cursorPosition;

			window.render();
		}

		Spec::terminate();
	}
	catch (const std::runtime_error& e){
		std::cout << "ERROR: " << e.what() << std::endl;
	}

	return 0;
}
