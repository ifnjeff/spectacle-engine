#ifndef MATERIAL_H
#define MATERIAL_H

#include <GL/glew.h>
#include <glm/glm.hpp>

#include "technique.h"
#include "texture.h"


namespace Spec {

	class Material {
	public:
		Material(glm::vec3 specularColor, float shininess, const Texture* diffuseMap, const Texture* normalMap, const Texture* specularMap);
		~Material();
		Material& operator= (const Material& other);
		Material(const Material& other);

		void changeSpecularColor(glm::vec3 specularColor);
		void changeShininess(float shininess);

		void changeDiffuseMap(const Texture& diffuseMap);
		void changeNormalMap(const Texture& normalMap);
		void changeSpecularMap(const Texture& specularMap);

		glm::vec3 getSpecularColor() const;
		float getShininess() const;

		void useUniforms() const;

	private:
		void createUbo();

		GLuint ubo;

		glm::vec3 specularColor;
		float shininess;

		const Texture* diffuseMap;
		const Texture* normalMap;
		const Texture* specularMap;

	};

}

#endif