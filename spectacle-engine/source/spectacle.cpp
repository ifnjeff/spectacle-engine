
#include "spectacle.h"


namespace Spec {

	bool init(){
		if (glfwInit() == GL_FALSE){
			throw std::runtime_error("Failed to initialize GLFW");
		}

		return true;
	}


	void update(){
		glfwPollEvents();
	}
	

	void terminate(){
		glfwTerminate();
	}

}