
#include "technique.h"


namespace Spec {

	Technique::Technique(const Shader& vertexShader, const Shader& fragmentShader){
		programObject = glCreateProgram();
		if (programObject == 0){
			throw std::runtime_error("Failed to create program object");
		}

		glAttachShader(programObject, vertexShader.getObject());
		glAttachShader(programObject, fragmentShader.getObject());

		glBindAttribLocation(programObject, ATTRIB_BINDING_POSITION, "vertPosition");
		glBindAttribLocation(programObject, ATTRIB_BINDING_NORMAL, "vertNormal");
		glBindAttribLocation(programObject, ATTRIB_BINDING_UV, "vertUv");
		glBindAttribLocation(programObject, ATTRIB_BINDING_COLOR, "vertColor");
		glBindAttribLocation(programObject, ATTRIB_BINDING_TANGENT, "vertTangent");
		glBindAttribLocation(programObject, ATTRIB_BINDING_BITANGENT, "vertBitangent");

		glLinkProgram(programObject);

		GLint linkStatus;
		glGetProgramiv(programObject, GL_LINK_STATUS, &linkStatus);
		if (linkStatus == GL_FALSE) {
			std::string errorMessage = "Failed to link program:\n";

			GLint infoLogLength;
			glGetProgramiv(programObject, GL_INFO_LOG_LENGTH, &infoLogLength);
			char* infoLog = new char[infoLogLength];
			glGetProgramInfoLog(programObject, infoLogLength, NULL, infoLog);
			errorMessage += infoLog;
			delete[] infoLog;

			glDeleteProgram(programObject);
			programObject = 0;
			throw std::runtime_error(errorMessage);
		}

		glDetachShader(programObject, vertexShader.getObject());
		glDetachShader(programObject, fragmentShader.getObject());

		entityUniformIndex = glGetUniformBlockIndex(programObject, "EntityUniforms");
		cameraUniformIndex = glGetUniformBlockIndex(programObject, "CameraUniforms");
		materialUniformIndex = glGetUniformBlockIndex(programObject, "MaterialUniforms");
		lightsUniformIndex = glGetUniformBlockIndex(programObject, "LightsUniforms");

		glUniformBlockBinding(programObject, entityUniformIndex, UNIFORM_BINDING_ENTITY);
		glUniformBlockBinding(programObject, cameraUniformIndex, UNIFORM_BINDING_CAMERA);
		glUniformBlockBinding(programObject, materialUniformIndex, UNIFORM_BINDING_MATERIAL);
		glUniformBlockBinding(programObject, lightsUniformIndex, UNIFORM_BINDING_LIGHTS);

		GLint currentProgram;
		glGetIntegerv(GL_CURRENT_PROGRAM, &currentProgram);

		glUseProgram(programObject);
		glUniform1i(getUniformLocation("diffuseMap"), TEXTURE_INDEX_DIFFUSE);
		glUniform1i(getUniformLocation("normalMap"), TEXTURE_INDEX_NORMAL);
		glUniform1i(getUniformLocation("specularMap"), TEXTURE_INDEX_SPECULAR);
		glUseProgram(currentProgram);

	}


	Technique::~Technique(){
		glDeleteProgram(programObject);
	}


	GLuint Technique::getUniformLocation(std::string uniform) const {
		return glGetUniformLocation(programObject, uniform.c_str());
	}


	GLuint Technique::getEntityUniformIndex() const {
		return entityUniformIndex;
	}


	GLuint Technique::getCameraUniformIndex() const {
		return cameraUniformIndex;
	}


	GLuint Technique::getMaterialUniformIndex() const {
		return materialUniformIndex;
	}


	GLuint Technique::getLightsUniformIndex() const {
		return lightsUniformIndex;
	}


	GLuint Technique::getObject() const {
		return programObject;
	}


	void Technique::use() const {
		glUseProgram(programObject);
	}

}
