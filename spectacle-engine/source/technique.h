#ifndef TECHNIQUE_H
#define TECHNIQUE_H

#include <GL/glew.h>

#include "shader.h"

#include <string>


namespace Spec {

	enum AttribBinding {
		ATTRIB_BINDING_POSITION = 0,
		ATTRIB_BINDING_NORMAL = 1,
		ATTRIB_BINDING_UV = 2,
		ATTRIB_BINDING_COLOR = 3,
		ATTRIB_BINDING_TANGENT = 4,
		ATTRIB_BINDING_BITANGENT = 5
	};

	enum UniformBinding {
		UNIFORM_BINDING_ENTITY = 0,
		UNIFORM_BINDING_CAMERA = 1,
		UNIFORM_BINDING_MATERIAL = 2,
		UNIFORM_BINDING_LIGHTS = 3
	};

	enum UniformOffset {
		UNIFORM_OFFSET_MVP = 0,
		UNIFORM_OFFSET_MV = 64,
		UNIFORM_OFFSET_M = 128,
		UNIFORM_OFFSET_MV_T = 192,
		UNIFORM_OFFSET_MV_IT = 256,
		UNIFORM_OFFSET_VP = 0,
		UNIFORM_OFFSET_V = 64,
		UNIFORM_OFFSET_P = 128,
		UNIFORM_OFFSET_SPEC_COLOR = 0,
		UNIFORM_OFFSET_SHININESS = 12,
		UNIFORM_OFFSET_NUM_LIGHTS = 0,
		UNIFORM_OFFSET_LIGHT_COLOR = 16,
		UNIFORM_OFFSET_LIGHT_POSITION = 144,
		UNIFORM_OFFSET_LIGHT_ATTENUATION = 272
	};

	enum UniformSize {
		UNIFORM_SIZE_ENTITY = 320,
		UNIFORM_SIZE_CAMERA = 192,
		UNIFORM_SIZE_MATERIAL = 16,
		UNIFORM_SIZE_LIGHTS = 400,
		UNIFORM_SIZE_LIGHT_STRUCT = 16
	};

	enum TextureUnit {
		TEXTURE_UNIT_DIFFUSE = GL_TEXTURE0,
		TEXTURE_UNIT_NORMAL = GL_TEXTURE1,
		TEXTURE_UNIT_SPECULAR = GL_TEXTURE2
	};

	enum TextureIndex {
		TEXTURE_INDEX_DIFFUSE = 0,
		TEXTURE_INDEX_NORMAL = 1,
		TEXTURE_INDEX_SPECULAR = 2,
	};

	class Technique {
	public:
		Technique(const Shader& vertexShader, const Shader& fragmentShader);
		~Technique();
		
		GLuint getUniformLocation(std::string uniform) const;
		GLuint getEntityUniformIndex() const;
		GLuint getCameraUniformIndex() const;
		GLuint getMaterialUniformIndex() const;
		GLuint getLightsUniformIndex() const;

		GLuint getObject() const;

		void use() const;

	private:
		Technique& operator= (const Technique& other);
		Technique(const Technique& other);

		GLuint programObject;

		GLuint entityUniformIndex;
		GLuint cameraUniformIndex;
		GLuint materialUniformIndex;
		GLuint lightsUniformIndex;

	};

}

#endif
