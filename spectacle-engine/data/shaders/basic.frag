in vec2 fragUv;

out vec4 color;

void main(){
	color = texture2D(diffuseMap, fragUv);
}