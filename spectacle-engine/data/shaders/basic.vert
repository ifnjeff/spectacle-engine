out vec2 fragUv;

void main(){
	fragUv = vertUv;
	gl_Position = mvp * vec4(vertPosition, 1.0);
}