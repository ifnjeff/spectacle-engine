in vec2 fragUv;
in vec3 fragPosition;
in vec3 fragNormal;

out vec4 color;

void main(){
	vec3 totalAmbient = vec3(0.0, 0.0, 0.0);
	vec3 totalDiffuse = vec3(0.0, 0.0, 0.0);
	vec3 totalSpecular = vec3(0.0, 0.0, 0.0);
	
	vec3 normal = normalize(fragNormal);
	
	for (int i = 0; i < numLights; i++){
		vec3 lightVector = lightPositions[i] - fragPosition;
		float falloff = lightAttenuations[i] * ((lightVector.x * lightVector.x) +
												(lightVector.y * lightVector.y) +
												(lightVector.z * lightVector.z));
		vec3 lightDirection = normalize(lightVector);
		
		float diffuse = max(dot(lightDirection, normal), 0.0);
		float specular = 0.0;
		
		color = vec4(lightDirection, 1.0);
		if (diffuse > 0.0){
			vec3 cameraDirection = normalize(-fragPosition);
			vec3 halfwayDirection = normalize(lightDirection + cameraDirection);
			float specularAngle = max(dot(halfwayDirection, normal), 0.0);
			specular = pow(specularAngle, shininess);
		}
		
		//totalAmbient += lightColors[i] * lights[i].ambientCoefficient;
		//totalDiffuse += (diffuse / falloff) * lightColors[i];
		totalSpecular += specular * lightColors[i];
	}
	
	/*color = vec4(totalAmbient + 
				 totalDiffuse * vec3(texture2D(diffuseMap, fragUv)) + 
				 totalSpecular * specularColor, 1.0);*/
}