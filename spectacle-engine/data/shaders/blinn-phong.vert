out vec2 fragUv;
out vec3 fragPosition;
out vec3 fragNormal;

void main(){
	fragUv = vertUv;
	fragPosition = vec3(m * vec4(vertPosition, 1.0));
	fragNormal = vec3(mv_it * vec4(vertNormal, 1.0));
	gl_Position = mvp * vec4(vertPosition, 1.0);
}